<?php

namespace app\common\traits;

/**
 * Soft delete trait for Yii2 ActiveRecord
 * Adapted for current tasks.
 *
 * @copyright (c) 2015, Oleg Poludnenko
 * @copyright (c) 2018, Pavel Yakupov
 * @license https://opensource.org/licenses/MIT MIT
 *
 * @property-read bool $isDeleted
 */
trait SoftDelete
{
    /**
     * Get deleted entity
     *
     * @param $id
     * @return mixed
     */
    public static function getDeletedEntity($id)
    {
        self::$softDelete = false;

        return self::findOne($id);
    }

    /**
     * Do not delete
     *
     * @var bool
     */
    public static $softDelete = true;

    /**
     * Returns newly created ActiveQuery instance
     *
     * @return \yii\db\ActiveQuery
     */
    public static function find()
    {
        $query = parent::find();

        // Skip deleted items
        if (static::$softDelete) {
            $query->andWhere([static::tableName() . '.' . static::$softDeleteAttribute => null]);
        }

        return $query;
    }

    /**
     * Deletes an ActiveRecord without considering transaction
     *
     * @return integer|false the number of rows deleted, or false
     * if the deletion is unsuccessful for some reason
     */
    protected function deleteInternal()
    {
        // Mark as deleted
        if (static::$softDelete) {
            $this->remove();
            $result = 1;
        } else {
            // Real delete
            $result = parent::deleteInternal();
        }

        return $result;
    }

    /**
     * Remove (aka soft-delete) record
     */
    public function remove()
    {
        // Evaluate timestamp and set attribute
        $timestamp = date('Y:m:d H:i:s');
        $attribute = static::$softDeleteAttribute;
        $this->$attribute = $timestamp;
        $trigger = static::$softDeleteTrigger;
        $this->$trigger = true;

        // Save record
        $this->save(false, [$attribute, $trigger]);

        // Trigger after delete
        $this->afterDelete();
    }

    /**
     * Restore soft-deleted record
     */
    public function restore()
    {
        // Mark attribute as null
        $attribute = static::$softDeleteAttribute;
        $this->$attribute = null;
        $trigger= static::$softDeleteTrigger;
        $this->$trigger = false;

        // Save record
        $this->save(false, [$attribute, $trigger]);
    }

    /**
     * Delete record from database regardless of the $softDelete attribute
     */
    public function forceDelete()
    {
        $softDelete = static::$softDelete;
        static::$softDelete = false;

        $this->delete();
        static::$softDelete = $softDelete;
    }

    /**
     * Returns if property is soft-deleted
     *
     * @return bool
     */
    public function getIsDeleted()
    {
        $attribute = static::$softDeleteAttribute;
        $idDeleted = $this->$attribute !== null;

        return $idDeleted;
    }
}