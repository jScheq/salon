<?php

namespace app\modules\api\controllers;

use Yii;
use yii\web\HttpException;
use app\modules\api\models\DealersFormat;

/**
 * Default controller for the `dealer` entity
 */
class DealerController extends ApiController
{
    public $modelClass = 'app\modules\api\models\DealersFormat';

    /**
     * Overwrite active controller delete for soft deleting
     *
     * @param $id
     * @throws HttpException
     */
    public function actionDelete($id) {
        $dealer = DealersFormat::findOne($id);
        if ($dealer) {
            $dealer->remove();

            Yii::$app->response->statusCode = 200;
        } else {
            throw new HttpException(404);
        }
    }

    /**
     * Cancel method for soft deleting
     *
     * @param $id
     * @return mixed
     * @throws HttpException
     */
    public function actionRestore($id)
    {
        $dealer = DealersFormat::getDeletedEntity($id);

        if ($dealer) {
            $dealer->restore();

            return $dealer;
        } else {
            throw new HttpException(404);
        }
    }
}
