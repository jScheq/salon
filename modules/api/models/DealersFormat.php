<?php

namespace app\modules\api\models;

use app\helpers\DateTransform;
use app\models\Dealers;

class DealersFormat extends Dealers
{
    public function rules()
    {
        $basicRules = $this->getBasicRules();

        $specialRules = [
            [['name', 'created', 'cars'], 'safe'],
        ];

        return array_merge($basicRules, $specialRules);
    }

    public function fields()
    {
        return [
            'id' => 'dealer_id',
            'name' => 'dealer_name',
            'created' => 'dealer_creation_year',
            'cars' => 'dealer_cars_amount',
            'createdAt' => function($model) {
                return DateTransform::toISO8601($model->dealer_created_at);
            },
            'updatedAt' => function($model) {
                return DateTransform::toISO8601($model->dealer_updated_at);
            },
        ];
    }

    public function setCreated($value)
    {
        $this->dealer_creation_year = $value;
    }

    public function setName($value)
    {
        $this->dealer_name = $value;
    }

    public function setCars($value)
    {
        $this->dealer_cars_amount = $value;
    }
}