<?php

use yii\db\Migration;

/**
 * Handles the creation of table `dealers`.
 */
class m180627_140150_create_dealers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('dealers', [
            'dealer_id' => $this->primaryKey(),
            'dealer_creation_year' => $this->integer(),
            'dealer_cars_amount' => $this->bigInteger(),
            'dealer_name' => $this->string()->notNull(),
            'dealer_is_deleted' => $this->boolean()->defaultValue(false),
            'dealer_created_at' => $this->dateTime()->notNull(),
            'dealer_updated_at' => $this->dateTime()->notNull(),
            'dealer_deleted_at' => $this->dateTime()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('dealers');
    }
}
