<?php

namespace app\helpers;

use DateTime;

class DateTransform
{
    /**
     * Конвертирует дату в формат ISO8601
     *
     * @param $date
     * @return null|string
     */
    public static function toISO8601($date): ?string
    {
        return $date
            ? (new DateTime( is_integer($date) ? date("Y-m-d H:i:s", $date) : $date))->format(DateTime::ISO8601)
            : null;
    }
}