<?php

namespace app\models;

use app\common\traits\SoftDelete;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "dealers".
 *
 * @property int $dealer_id
 * @property int $dealer_creation_year
 * @property int $dealer_cars_amount
 * @property string $dealer_name
 * @property int $dealer_is_deleted
 * @property string $dealer_created_at
 * @property string $dealer_updated_at
 * @property string $dealer_deleted_at
 */
class Dealers extends ActiveRecord
{
    use SoftDelete;

    /**
     * Removal time
     *
     * @var string
     */
    public static $softDeleteAttribute = 'dealer_deleted_at';

    /**
     * Delete flag
     *
     * @var string
     */
    public static $softDeleteTrigger = 'dealer_is_deleted';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dealers';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'dealer_created_at',
                'updatedAtAttribute' => 'dealer_updated_at',
                'value' => function() {
                    return date('Y-m-d H:i:s');
                }
            ],
        ];

    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return $this->getBasicRules();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'dealer_id' => 'Dealer ID',
            'dealer_creation_year' => 'Dealer Creation Year',
            'dealer_cars_amount' => 'Dealer Cars Amount',
            'dealer_name' => 'Dealer Name',
            'dealer_is_deleted' => 'Dealer Is Deleted',
            'dealer_created_at' => 'Dealer Created At',
            'dealer_updated_at' => 'Dealer Updated At',
            'dealer_deleted_at' => 'Dealer Deleted At',
        ];
    }

    protected function getBasicRules(): array {
        return [
            [['dealer_creation_year', 'dealer_cars_amount'], 'integer'],
            [['dealer_name'], 'required'],
            [['dealer_name'], 'string', 'max' => 255],
        ];
    }
}
